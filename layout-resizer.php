<?php
// PROFFESIONAL CHEAT SHEET
// mdpi		160 dpi		1x	
// hdpi		240 dpi		1.5x
// xhdpi	320 dpi		2x
// xxhdpi	480 dpi		3x
// xxxhdpi	640 dpi		4x

// Set your mode here
// 0 - Portrait
// 1 - Landscape
// 2 - Both
$mode = 2;

// Set your file name here
$file_name = "dialog_cancel.xml";

// Units
$units = array("dp"/*, "sp"*/);

$files = array("layout\\".$file_name, "layout-land\\".$file_name);

$dir_names = array(
	0 => array("layout-mdpi", "layout-hdpi", "layout-xhdpi", "layout-xxhdpi", "layout-xxxhdpi"),
	1 => array("layout-land-mdpi", "layout-land-hdpi", "layout-land-xhdpi", "layout-land-xxhdpi", "layout-land-xxxhdpi"),
	2 => array("layout-mdpi", "layout-hdpi", "layout-xhdpi", "layout-xxhdpi", "layout-xxxhdpi", "layout-land-mdpi", "layout-land-hdpi", "layout-land-xhdpi", "layout-land-xxhdpi", "layout-land-xxxhdpi")
);

$modifyMe = array();
$modified = array();

checkDirs($dir_names[$mode]);
copyFiles($dir_names, $files, $mode);

foreach ($dir_names[$mode] as $dir)
{
	if (!$file_name)
	{
		break;
	}
	
	$sizeForFolder = setSizeForFolder($dir);
	$targetFile = $dir."\\".$file_name;
	
	foreach ($units as $absoluteUnits)
	{
		$target = getLineWithString($targetFile, $absoluteUnits);
		
		foreach ($target as $targets)
		{	
			$sliced = getInbetweenStrings('"', $absoluteUnits.'"', $targets);
			foreach ($sliced as $slice)
			{
				array_push($modifyMe, $slice);
			}
		}

		foreach ($modifyMe as $modifiers)
		{
			array_push($modified, (int) $modifiers * $sizeForFolder);
		}

		for ($i = 0; $i < count($modifyMe); $i++)
		{
			$ogStringToReplaceWith = '"'.$modifyMe[$i].$absoluteUnits.'"';
			$replaceMeWithThis = '"'.$modified[$i].$absoluteUnits.'"';
			EditFileContent($targetFile, $ogStringToReplaceWith, $replaceMeWithThis);
		}
		$modifyMe = array();
		$modified = array();
	
	}
}

ECHO "DAMMIT IM HOMESICK";

function EditFileContent($file, $string_to_replace, $replace_with)
{
	$content=file_get_contents($file);
	$content_chunks=explode($string_to_replace, $content);
	$content=implode($replace_with, $content_chunks);
	file_put_contents($file, $content);
}

function getLineWithString($file, $units)
{
    $lines = file($file);
	$counter = 0;
	$dp = array();
	
    foreach ($lines as $lineNumber => $line)
	{
        if (strpos($line, $units) !== false) 
		{
			array_push($dp, $line);
        }
    }
	return $dp;
}

function checkDirs($dir_names)
{
	$dirs = array_filter(glob('*'), 'is_dir');
	
	foreach ($dir_names as $dir_name)
	{
		if (!in_array($dir_name, $dirs))
		{
			exec('mkdir '.$dir_name);
		}
	}
}

function copyFiles($dir_names, $files, $mode)
{
	switch ($mode)
	{
		// Portrait
		case 0:
			$i = 0;
			$count = 1;
		break;
		
		// Landscape
		case 1:
			$i = 1;
			$count = 2;
		break;
		
		// Both
		case 2:
			$i = 0;
			$count = 2;
		break;
	}
	for ($i; $i < $count; $i++)
	{
		$fileToCopy = $files[$i];
		$directoriesToCopy = $dir_names[$i];
		for ($j = 0; $j < count($directoriesToCopy); $j++)
		{
			exec('COPY '.$fileToCopy.' '.$directoriesToCopy[$j]);
		}
	}
	
}

function setSizeForFolder($dir_name)
{	
	$values;
	
	if ((preg_match('/\land\b/', $dir_name)))
	{
		$values = getInbetweenStrings('-land-', '', $dir_name);
	}
	else
	{
		$values = getInbetweenStrings('-', '', $dir_name);
	}
	foreach ($values as $value)
	{
		switch ($value)
		{
			case "mdpi":
				return 1;
			break;
			
			case "hdpi":
				return 1.5;
			break;
			
			case "xhdpi":
				return 2;
			break;
			
			case "xxhdpi":
				return 3;
			break;
			
			case "xxxhdpi":
				return 4;
			break;		
		}
	}
}
	
function getInbetweenStrings($start, $end, $name)
{
    $matches = array();
    $regex = "/$start([a-zA-Z0-9_]*)$end/";
    preg_match_all($regex, $name, $matches);
    return $matches[1];
}

?>